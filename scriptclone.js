$('document').ready(function(){

    // 1. AJAX CALL
    let url = 'http://data.metromobilite.fr/api/routers/default/index/routes';
    
    $.ajax({
        url: url,
        type: "GET", // par défaut
        dataType: "json", // ou HTML par ex
        
        // Si succès 
        success: function(data, statut) {
            console.warn("SUCCESS");
            // console.log(data);
            console.log('Statut : ' + statut);
            
            // Ma fonction => ne fonctionne que chez moi ^^
            listing(data);
            
        },
        
        // Si erreur
        error: function(result, statut, erreur){
            console.warn("ERREUR");
            console.log(result);
            console.log("Erreur : " + statut + ' ' + erreur)
        },
        
        // A la fin !
        complete: function(result, statut){
            console.warn("AJAX CALL COMPLETE");
            console.log(result);
            console.log("Complet : " + statut);
        }
        
    });
    
    // 2. Mes fonctions persos
    
    function listing(data){
        console.log(data)

        for(let i = 0; i < data.length; i++){

            let nomDeLigneLongue = data[i].longName;
            let nomDeLigneCourte = data[i].shortName;
            let modeDeLigne = data[i].mode;
            let couleurLigne = data[i].color;

            console.log( nomDeLigneLongue + ' - ' + data[i].type + ' - #' + data[i].color);

            let nom = "<h2>" + nomDeLigneLongue + "</h2>";
            $('#contentBus').append(nomDeLigneLongue); //injecte de nomDeLigneLongue sur html id contentBus 
            $('#contentBus').css('display', 'block');

            // .html('<p>lkjjflskdfjlsdkfj</p>')
            // .text("du texte")

            modeDeLigne = document.createElement('tr');
            modeDeLigne.innerText = data[i].mode;
            document.body.append (modeDeLigne);

            nomDeLigneCourte = document.createElement('button');
            nomDeLigneCourte.innerText = data[i].shortName;
            //nomDeLigneCourte.innerHTML = data[i].color;
            document.body.append (nomDeLigneCourte);
            $('button').addClass('infoBusClick box').attr('id' , data[i].id);
            // $('.infoBusClick').click(function(){$(s'.infoBus').show();});
            $('button').css('display', 'inline-flex');


            nomDeLigneLongue = document.createElement('tr'); //creation de tr pour nom longue
            nomDeLigneLongue.innerText = data[i].longName; // ajout à HTML
            document.body.append (nomDeLigneLongue); //affichage sur html
            $('tr').addClass('infoBus') //ajout de la class
            $('tr').css('display','none') //ajout display none pour div de nom longue

            
        }

        $('.box').click(function(){
            alert('ok')
    
                    let idTransport = $(this).attr('id');
    
                    let urlId = 'http://data.metromobilite.fr/api/routers/default/index/routes?codes=' + idTransport;
                    
    
                    $.ajax({
                        url: urlId,
                        type: "GET", // par défaut
                        dataType: "json", // ou HTML par ex
                        
                        // Si succès 
                        success: function(data, statut) {
                            console.warn("SUCCESS detail");
                            console.log(data);
                            console.log('Statut : ' + statut);
    
                            
                            // Ma fonction => ne fonctionne que chez moi ^^
                            $('.infoBus').css('display','block')
                            
                        },
                        
                        // Si erreur
                        error: function(result, statut, erreur){
                            console.warn("ERREUR");
                            console.log(result);
                            console.log("Erreur : " + statut + ' ' + erreur)
                        },
                        
                        // A la fin !
                        complete: function(result, statut){
                            console.warn("AJAX CALL COMPLETE");
                            console.log(result);
                            console.log("Complet : " + statut);
                        }
                        
                    });
                });
    
    }
    
    


        });

        // if (typeDeLigne == 'TRAM'){ //rewrite to switch-case

        //     // Création button avec son id + son style
        //     // tu peux utiliser les `${var}` ou le ' + var + '
        //     // mettre l'ID dans une variable

        //     $('#contentTram').append('<button id="' + data[i].id + '" style="background: #' + couleurLigne+'">'+ nomDeLigneCourte + '</button>');  //add color with id for each button; couleurLigne - color's variable, nomDeLigneCourte - shortname's variable 
        //     $('#contentTram').append('<p>' + nomDeLigneLongue + '</p>');
        //     //$('#infoTram').append('<p id="' + data[i].id + '">'+ nomDeLigneLongue +'</p>');
           
        //   }

        // if (typeDeLigne == 'SNC'){
        //     $('#contentTrain').append('<button id="' + data[i].id + '" style="background: #' + couleurLigne+'">'+ nomDeLigneCourte + '</button>'); 
        //     $('#contentTrain').append('<p>' + nomDeLigneLongue + '</p>');
            
        //   }

        // if (typeDeLigne == 'FLEXO' || typeDeLigne=='Structurantes' || typeDeLigne=='Urbaines'
        //   || typeDeLigne=='Interurbaines') {
        //      $('#contentBus').append('<button id="'+data[i].id+'">' + nomDeLigneCourte + '</button>');
        //      $('#contentBus').append('<p>' + nomDeLigneLongue + '</p>');
        //    }
        

        // if (typeDeLigne == 'C38') {
        //     $('#contentBusExpress').append('<button id="' + data[i].id + '" style="background: #' + couleurLigne+'">'+ nomDeLigneCourte + '</button>');
        //     $('#contentBusExpress').append('<p>' + nomDeLigneLongue + '</p>');
        //     }  
        
        // if (typeDeLigne == 'SCOL') {
        //     $('#contentBusScolaires').append('<button id="' + data[i].id + '" style="background: #' + couleurLigne+'">'+ nomDeLigneCourte + '</button>');
        //     $('#contentBusScolaires').append('<p>' + nomDeLigneLongue + '</p>');
        //     }   


        const geoJsonGre = ''; // TODO : à remplir

const mymap = L.map('laCarte').setView([51.505, -0.09], 7);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png').addTo(mymap);

L.marker([51.505, -0.09]).addTo(mymap).bindPopup('TOTO').openPopup();

L.marker([51.505, -0.12]).addTo(mymap).bindPopup('TATA').openPopup();

L.polygon([
    [51.505, -0.09],
    [51.505, -0.12]
]).addTo(mymap);

L.geoJSON(geoJsonGre, {
    style: {
        color: '#777333'
    }
}).addTo(mymap);