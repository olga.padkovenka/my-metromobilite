//fonction calcul les horraires 

function convert(t) {
    
    hours = Math.floor(t / 3600);
    mins = Math.floor((t - hours*3600) / 60);
    console.log('convert : ' + hours.toString() + ":" + mins.toString());
    return hours.toString() + ":" + mins.toString();
    
}

const maCarte = L.map('maCarte').setView([45.191816, 5.721656], 12);


$('document').ready(function(){

    // 1. AJAX CALL
    let url = 'http://data.metromobilite.fr/api/routers/default/index/routes';
    
    $.ajax({
        url: url,
        type: "GET", // par défaut
        dataType: "json", // ou HTML par ex
        
        // Si succès 
        success: function(data, statut) {
            console.log(data)
            console.warn("SUCCESS");
            // console.log(data);
            console.log('Statut : ' + statut);
            
            // Ma fonction => ne fonctionne que chez moi ^^
            listing(data);
            
        },
        
        // Si erreur
        error: function(result, statut, erreur){ //add "try again" button
            console.warn("ERREUR");
            console.log(result);
            console.log("Erreur : " + statut + ' ' + erreur)

            let buttonRestart = '<button id="buttonRestart"> Button restart </button>' //bouton pour recharger la page

            $(buttonRestart).click(function() { //fonction qui recharge la page
                    location.reload();
                 });
                
        },
        
        // A la fin !
        complete: function(result, statut){
            console.warn("AJAX CALL COMPLETE");
            console.log(result);
            console.log("Complet : " + statut);
        }
        
    });
 
    // 2. Mes fonctions persos
    
    function listing(data){
        console.log(data)

        for(let i = 0; i < data.length; i++){ //ma boucle

            let nomDeLigneLongue = data[i].longName; //mes variables
            let nomDeLigneCourte = data[i].shortName;
            let couleurLigne = data[i].color;
            let typeDeLigne = data[i].type;

            console.log(data[i].id)

            let btn = '<button id="'+ data[i].id +'" class="box"></button>'; //variable btn crée des boutons avec ajout d'un id pour chaque bouton et class box pour chaque bouton
            
            $(btn).attr('id'); //ajout à ma variable btn un id 

          
            
            console.log(couleurLigne)

            //si typeDeLigne est TRAM etc, je crée un bouton avec un id, je récupère background color et nom de la ligne court

            switch(typeDeLigne) {

                case 'TRAM' :
                     $('#contentTram').append('<button id="' + data[i].id + '" style="background: #' + couleurLigne+'">'+ nomDeLigneCourte + '</button>'); 
                     $('#contentTram').append('<p id="info_' + data[i].id +'">' + '<img class="imgTramLogo" src="images/tram_PNG20.png">'+ '</img>' + nomDeLigneLongue + '</p>'); //ajout un id pour chaque <p> de contentTram

                     break;

                case 'SNC' :
                    $('#contentTrain').append('<button id="' + data[i].id + '" style="background: #' + couleurLigne+'">'+ nomDeLigneCourte + '</button>'); 
                    $('#contentTrain').append('<p id="info_' + data[i].id + '" >' + nomDeLigneLongue + '</p>');
                    break;

                case 'FLEXO' , 'Structurantes' , 'Urbaines' , 'Interurbaines':
                    $('#contentBus').append('<button id="'+data[i].id+'">' + nomDeLigneCourte + '</button>');
                    $('#contentBus').append('<p id="info_' + data[i].id + '" >' + nomDeLigneLongue + '</p>');
                    break;

                case 'C38' : 
                    $('#contentBusExpress').append('<button id="' + data[i].id + '" style="background: #' + couleurLigne+'">'+ nomDeLigneCourte + '</button>');
                    $('#contentBusExpress').append('<p id="info_' + data[i].id + '" >' + nomDeLigneLongue + '</p>');
                    break;

                case 'SCOL' :
                    $('#contentBusScolaires').append('<button id="' + data[i].id + '" style="background: #' + couleurLigne+'">'+ nomDeLigneCourte + '</button>');
                    $('#contentBusScolaires').append('<p id="info_' + data[i].id + '" >' + nomDeLigneLongue + '</p>');
                    break;

            } //fin de switch


         } //fin de boucle

         $('#contentTram').css('display', 'none'); 
         $('#contentTrain').css('display', 'none');
         $('#contentBus').css('display', 'none');
         $('#contentBusExpress').css('display', 'none');
         $('#contentBusScolaires').css('display', 'none');
         //$('#infoTram').css('display', 'none');

    $('#tramLines').click(function(){ //si je clique sur #tramLines, ça m'affiche contentTram 
         $('#contentTram').toggle()
     })

    $('#trainLines').click(function(){
         $('#contentTrain').toggle()

     })

    $('#busLines').click(function(){
         $('#contentBus').toggle()

     })

    $('#busExpressLines').click(function(){
         $('#contentBusExpress').toggle()

     })

    $('#busScolaireLines').click(function(){
         $('#contentBusScolaires').toggle()

     }) 


        $('p').addClass('contentInfo'); //add class contentInfo to <p>
        $('button').addClass('contentButton'); //add class contentButton to button
        $('.contentInfo').hide(); // hide class contentInfo by default

        $('.contentButton').click(function () { 
            //i click on button with class contentButton 

            let stopsId = $(this).attr('id'); //je recupere des id pour afficher des arrets
            //console.log(stopsId)
            
            let urlStops = 'https://data.metromobilite.fr/api/ficheHoraires/json?route=' + stopsId;

            
            $.ajax({
                url: urlStops,
                type: "GET", // par défaut
                dataType: "json", // ou HTML par ex
                
                // Si succès 
                success: function(data, statut) {
                    console.warn("SUCCESS Stops");
                    // console.log(data);
                    console.log('Statut : ' + statut);
                    
                    // Ma fonction => ne fonctionne que chez moi ^^
                    listingStops (data);
                    
                },
                
            });

            let info = $(this).next('.contentInfo'); //veriable info add next object after contentInfo. It's <p> 
            if (info.css('display') == 'none') //if my veriable info have display none, 
              {
              $('.contentInfo').hide(); //i hide <p> with class contentInfo
              info.show(); //i show <p> with class info 
            }
            else {
              info.hide(); //else i hide info
            }

          }); //fin de function anonime quand je clique sur les buttons avec class contentButton 

    }; //fin de function listing(data)  

    function listingStops (data){
        console.log(data)
        $('.divStops').text('') //supprimer le text à l'intérieur de la divStops 
        $('.divHorraires').text('');
        for(let i = 0; i < data[0].arrets.length; i++){

            // let maCarte = $('#maCarte');
            let mesArrets = data[0].arrets[i]['stopName']
            let mesHorraires = data[0].arrets[i]['trips'] // je recupere trips (horraire) 
            console.log(mesArrets)
            
            let infoStops = document.createElement('div');
            $(infoStops).addClass('divStops');

            let infoHorraires = document.createElement('div');
            $(infoHorraires).addClass('divHorraires');
            
            $(infoStops).append('<p class="stops">' + mesArrets + '</p>'); 
            
            L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png').addTo(maCarte);

            $('.contentInfo').append(infoStops);           
            
            //ma boucle pour parcourir le tableau des horraires convert 
            
            for(let i = 0; i < mesHorraires.length; i++ ){
                
                //appel de ma fonction pour convertir des horraires
                $(infoHorraires).append('<p class="horraires">' + convert(data[0].arrets[i]['trips'][i]) + '</p>'); 
            }
            $('.contentInfo').append(infoHorraires)
     
        } // fin de ma boucle for

    } //fin de ma fonction listingStops

   

     // fenetre modale quand la page se charge
     $(document).ready(function() {
        // fermeture du lien 
        $('.popup-close').click(function() {
            $(this).parents('.popup-fade').fadeOut();
            return false;
        }); 

    // fonction qui permet d'afficher le message chaque minute    
        setInterval(function(){
            $('.popup-close').click(function() {
                $(this).parents('.popup-fade').fadeIn();
                return false;
            });
        }, 100000); //each 10 minites

    }); //fin de ready(function) fenetre modale

        }); // fin de ready(function)
